import React from 'react'
import {  useBox } from '@react-three/cannon'

const Plane = ({position=[0,0,0] }) => {
    const [ref] = useBox(() => ({
        position,
        args:[20,0.001,20]
    }))
    return (
        <mesh ref={ref} >
            <boxBufferGeometry attach='geometry' args={[20,0.001,20] }/>
            <meshLambertMaterial attach='material' color='lightblue' />
        </mesh>
    )
}
export default Plane
