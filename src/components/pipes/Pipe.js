import React, { useMemo } from 'react'
import {  useThree } from 'react-three-fiber';
import { SinglePipe } from './SinglePipe';

const Pipe = ({ startingPosition = [6, 2, 0] }) => {
    const arr = Array.apply(null, { length: 15 }).map(e => Math.random() * 3.5 + 0.2);
    const { viewport } = useThree();

    const distance = useMemo(()=>viewport.height / 1.75,[viewport.height]);
    return (
        <group castShadow receiveShadow>
            <SinglePipe position={startingPosition} arr={arr} />
            <SinglePipe position={[startingPosition[0], startingPosition[1] - distance, startingPosition[2]]} rotation={[0, 0, -Math.PI]} arr={arr} distance={distance} />
        </group>
    )
}

export default Pipe
