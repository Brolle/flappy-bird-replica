import './App.css';
import React from 'react';
import { BrowserRouter, Redirect, Route, Switch } from 'react-router-dom';
import FlappyBirdScene from './components/FlappyBirdScene';

function App() {
  
  return (
    <div className='app' >
      <BrowserRouter>
        <Switch>
          <Route path='/flappy-bird'>
            <FlappyBirdScene />
          </Route>
          <Redirect path='*' exact to='/flappy-bird' />
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
