# React Flappy Bird

Flappy bird implementation created with react three fiber

Here you can try [the game](http://ReactFlappyBird.surge.sh)

This is my first project in React Three Fiber and i am happy with how it turned out but there is still work to be done!

List of things to be added:

- Add mobile compatibility
- Option for different camera angle
- Make a gltf model of the bird
- Improve styling
- Improve responsiveness
